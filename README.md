# polis

A dashboard for crime information

## Configuration

This service uses the following environment variables:

`PORT` - PORT number where the server will run (default: `11001`).

`PG_URI` - URI for the Postgres database (default: `localhost`)

`PG_DATABASE` - Posgres database name (default: `postgres`)

`PG_USERNAME` - username for the postgres database (default: `postgres`)

`PG_PASS` - password for the postgres database (default: `fastandbulbous`)

## Preparation

This app needs a Postgress database with the crime data loaded.

1. Extract files to the `input-data` directory

2. run `docker-compose up` to start a local dockerised Postgres database
(it may take a few minutes to download)

3. Connect to your database:
`psql postgresql://postgres:fastandbulbous@localhost:5432/postgres`

4. Create the table:
```
create table uk_crimes (crime_id text, month text, reported_by text, falls_within text, longitude text, latitude text, location text,  lsoa_code text, lsoa_name text, crime_type text, last_outcome_category text, context text);
```
5. Load the CSV data:
```
postgres=# \copy uk_crimes from 'input-data/2017-01/2017-01-essex-street.csv' with (format csv);
COPY 13307
postgres=# \copy uk_crimes from 'input-data/2017-02/2017-02-essex-street.csv' with (format csv);
COPY 12964
postgres=# \copy uk_crimes from 'input-data/2017-03/2017-03-essex-street.csv' with (format csv);
COPY 15155
postgres=# \copy uk_crimes from 'input-data/2017-04/2017-04-essex-street.csv' with (format csv);
COPY 14862
postgres=# \copy uk_crimes from 'input-data/2017-05/2017-05-essex-street.csv' with (format csv);
COPY 15717
postgres=# \copy uk_crimes from 'input-data/2017-06/2017-06-essex-street.csv' with (format csv);
COPY 15719
```
6. Cleanup the headers
```
postgres=# delete from uk_crimes where crime_id = 'Crime ID';
```

## Installation

You can install and use the [leiningen](http://leiningen.org/) tool to run, build and execute tests.


To run the tests, execute:

```
lein test
```

To generate the uberjar, execute:

```
$ lein uberjar
```

## Starting the server

### Start the server with the following command (using default values):

```
$ java -jar target/app.jar
```

Or simply:

```
lein run
```

Note: if you encounter errors running `lein test` or `lein run`, try executing:

`lein clean`

Or, to do both, `lein do clean, run` or `lein do clean, test`

### Using custom port and postgres password:

```
PORT=11111 PG_PASS=willitblend lein run
```

or

```
PORT=11111 PG_PASS=willitblend java -jar target/app.jar
```

## Examples

#### Show current configuration, access:

[http://localhost:11001/internal/config]

#### healthcheck endpoint, should return 200 "This is fine!"


[http://localhost:11001/internal/healthcheck]


#### See all crimes on database, access:

[http://localhost:11001/crimes]


#### See crimes filtered by crime type "Burglary"

[http://localhost:11001/crimes/bytype?q=Burglary]


#### See crimes filtered by crime type "Other theft"

[http://localhost:11001/crimes/bytype?q=Other%20theft]




## License

Copyright © 2017 Igor Garcia

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
