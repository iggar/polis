(ns polis.handler-test
  (:require [clojure.test :refer :all]
            [polis.handler :refer :all]))

(deftest crime-row-test
  (testing "test if row is being built correctly"
  (let [a-record {:lsoa_code "E01029880", :month "2017-01", :longitude "0.747825",
                 :lsoa_name "Babergh 006A", :crime_type "Burglary",
                 :last_outcome_category "Investigation complete; no suspect identified",
                 :context nil, :latitude "52.017984",
                 :crime_id "7395e66bd774f868e0d5ea15deb33f4990cd8dd62f1323055239456e392c8a79",
                 :falls_within "Essex Police", :reported_by "Essex Police",
                 :location "On or near Park/Open Space"}
        formatted-row [:tr [:td "2017-01"] [:td "52.017984,0.747825"] [:td "On or near Park/Open Space"] [:td "E01029880-Babergh 006A"] [:td "Burglary"] [:td nil]]]
    (is (= formatted-row (crime-row a-record))))))
