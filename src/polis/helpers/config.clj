(ns polis.helpers.config
  (:require
   [clojure.string :as string]
   [environ.core :refer [env]]
   [polis.helpers.config :as config]))

(def port (Integer/parseInt (or (env :port) "11001")))

(def pg-uri
  (env :pg-uri "localhost"))

(def pg-database
  (env :pg-database "postgres"))

(def pg-username
  (env :pg-username "postgres"))

(def pg-pass
  (env :pg-pass "fastandbulbous"))

(defn all
  "List all the config variables used"
  []
  (reduce-kv
    (fn [acc k v]
      (if-not (= 'all k)
        (merge
          acc {k (var-get v)})
        acc))
    {}
    (ns-publics 'polis.helpers.config)))
