(ns polis.database
  (:require
    [polis.helpers.config :as config]
    [clojure.java.jdbc :as j]))

(def get-all-crime-data-statement
  "SELECT * FROM uk_crimes LIMIT 100")

(def find-crime-data-month-statement
  "SELECT * FROM uk_crimes WHERE month = ? LIMIT 100")

(def find-crime-data-crime-type-statement
  "SELECT * FROM uk_crimes WHERE crime_type = ? LIMIT 100")

(def crimes-database-uri
  (str "postgresql://" config/pg-username ":" config/pg-pass "@" config/pg-uri "/" config/pg-database))

(defn all-crimes []
  (j/query crimes-database-uri get-all-crime-data-statement))

(defn all-crimes-by-type [crime-type]
  (j/query crimes-database-uri [find-crime-data-crime-type-statement crime-type]))
