(ns polis.main
(:require
 [org.httpkit.server :refer [run-server]]
 [clojure.tools.logging :as log]
 [polis.handler :as handler]
 [polis.database :as database]
 [polis.helpers.config :as config])
(:gen-class))

(defn -main []
   (log/info "Starting up on port: " config/port)
  (run-server handler/app {:port config/port}))
