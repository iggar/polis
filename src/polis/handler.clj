(ns polis.handler
  (:require [polis.helpers.config :as config]
            [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [polis.database :as db]
            [hiccup.page :refer [html5]]))

(defn crime-row [record]
  [:tr
   [:td (:month record)]
   [:td (str (:latitude record) "," (:longitude record))]
   [:td (:location record)]
   [:td (str (:lsoa_code record) "-" (:lsoa_name record))]
   [:td (:crime_type record)]
   [:td (:last_outcome record)]])

(defn crimes-table-template [lines]
  (html5 [:body
          [:h1 "Crime Stats Dashboard"]
          [:h2 "UK Crime information"]
          [:h3 "Current configuration"]
          [:table
           [:th "Month"]
           [:th "Geo location"]
           [:th "Location"]
           [:th "LSOA"]
           [:th "Crime type"]
           [:th "Last outcome"]
           lines]]))

(defn crimes-table []
  (crimes-table-template (map crime-row (db/all-crimes))))

(defn crimes-table-by-type [crime-type]
  (println "searching crime types of" crime-type)
  (crimes-table-template (map crime-row (db/all-crimes-by-type crime-type))))

(defapi app
  (GET "/internal/healthcheck" []
    (ok {:status "This is fine!"}))
  (GET "/internal/config" []
    (ok (config/all)))
  (GET "/crimes" []
    (ok
     (crimes-table)))
  (GET "/crimes/bytype" []
    :query-params [q :- String]
    (if-not (empty? q)
      (ok
       (crimes-table-by-type q))))
  (GET "/myconfig" []
    (ok "welcome")))
