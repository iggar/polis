(defproject polis "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]
                 [org.clojure/tools.logging "0.3.1"]
                 [metosin/compojure-api "1.1.11"]
                 [environ "1.0.3"]
                 [http-kit "2.1.18"]
                 [hiccup "1.0.5"]]
  :ring {:handler polis.handler/app}
  :main polis.main
  :uberjar-name "app.jar"
  :profiles {:uberjar {:aot [polis.main]}
             :dev {:dependencies [[cheshire "5.5.0"]
                                  [javax.servlet/servlet-api "2.5"]
                                  [org.clojars.runa/conjure "2.1.3"]
                                  [ring/ring-mock "0.3.0"]
                                  [metosin/compojure-api "1.1.1"]
                                  [hiccup "1.0.5"]]
                   :plugins [[lein-ring "0.9.7"]]}})
